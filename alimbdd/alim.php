<?php
    $serveur = "localhost";
    $dbname = "Food";
    $user = "lucas";
    $pass = "root";
    

    $nom = $_POST["nom"];
    $ig = $_POST["ig"];
    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$serveur;dbname=$dbname",$user,$pass);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        //On insére les données 
        $sth = $dbco->prepare("
        INSERT INTO menu(nom, ig)
        VALUES(:nom, :ig)");
        $sth->bindParam(':nom',$nom);
        $sth->bindParam(':ig',$ig);
        $sth->execute();

    }
    catch(PDOException $e){
        echo 'Erreur : '.$e->getMessage();
    }

    header('Location: ../admin/admin.php');
?>