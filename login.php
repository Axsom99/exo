
<?php
 if(isset($_POST['submit']) && $_POST['submit'] == 'SUBMIT'){
  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
  {
        $secret = '6Le9CssZAAAAAJhU27q74DBY540ANAVhycGAHnGq';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.
            $secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
        { ?>
<div style="color: limegreen;"><b>Your contact request have submitted successfully.</b></div>
        <?php }
        else
        {?>
            <div style="color: red;"><b>Robot verification failed, please try again.</b></div>
        <?php }
   }else{?>
       <div style="color: red;"><b>Please do the robot verification.</b></div>
   <?php }
 }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exo </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">

</head>
<body class="center">
<h1 class="h1">Formulaire de connexion</h1>
    <div class="card-body">
        <form class="col-md-12 form" action="log/log.php" method="post">
        <div class="alert"></div>
            <label>Pseudo</label>
            <input class="form-control" type="text" id="pseudo" name="pseudo" placeholder="Votre pseudo"
                required>
            <label>Mot de passe</label>
            <input class="form-control" type="password" id="password" name="password"
                placeholder="Votre mot de passe " required>
            <button type="submit" class="btn btn-outline-warning">Envoyer</button>
        </form>
       <a class="responsive" href="index.php"> <button id="lui" type="submit" class="btn btn-outline-warning">Accueil</button></a>
    </div>
    <div class="g-recaptcha" data-sitekey = "6LfFCcsZAAAAAPVIGYi6FbzI5TgmFaWw-B93boAS"></div>

   
    <script src ="https://www.google.com/recaptcha/api.js" différé asynchrone></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
